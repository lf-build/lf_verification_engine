﻿using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using RestSharp.Extensions;
using System.Collections.Generic;
using System.Net;
using System;
using System.Linq;
using System.Net.Http.Headers;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
#endif

namespace LendFoundry.VerificationEngine.Api
{
    /// <summary>
    /// VerificationEngineController class
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Services.ExtendedController" />
    [Route("/")]
    public class VerificationEngineController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VerificationEngineController"/> class.
        /// </summary>
        /// <param name="verificationEngineService">The verification engine service.</param>
        public VerificationEngineController(IVerificationEngineService verificationEngineService)
        {
            VerificationEngineService = verificationEngineService;
        }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        private IVerificationEngineService VerificationEngineService { get; }

        /// <summary>
        /// Verifies the facts.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="fact">The fact.</param>
        /// <param name="method">The method.</param>
        /// <param name="source">The source.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{workFlowStatusId}/{fact}/{method}/{source}")]
        [ProducesResponseType(typeof(IVerificationMethod), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> VerifyFacts(string entityType, string entityId, string workFlowStatusId, string fact, string method, string source, [FromBody]object request)
        {
            return await ExecuteAsync(async () =>
            {
                var decodedFactString = WebUtility.UrlDecode(fact);
                var decodedMethodString = WebUtility.UrlDecode(method);
                var decodedSourceString = WebUtility.UrlDecode(source);
                return Ok((await VerificationEngineService.Verify(entityType, entityId, workFlowStatusId, decodedFactString, decodedMethodString, decodedSourceString, request)));
            });
        }

        /// <summary>
        /// Gets the verification status.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="fact">The fact.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{workFlowStatusId}/{fact}/status")]
        [Produces("application/json", Type = typeof(IVerificationFacts))]
        [ProducesResponseType(typeof(IVerificationMethod), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]        
        public async Task<IActionResult> GetVerificationStatus(string entityType, string entityId, string workFlowStatusId, string fact)
        {
            return await ExecuteAsync(async () =>
            {
                var decodedFactString = WebUtility.UrlDecode(fact);
                return Ok((await VerificationEngineService.GetStatus(entityType, entityId, workFlowStatusId, decodedFactString)));
            });
        }

        /// <summary>
        /// Gets the verification detail.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="fact">The fact.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{workFlowStatusId}/{fact}/detail")]
        [ProducesResponseType(typeof(List<IVerificationMethod>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetVerificationDetail(string entityType, string entityId, string workFlowStatusId, string fact)
        {
            return await ExecuteAsync(async () =>
            {
                var decodedFactString = WebUtility.UrlDecode(fact);
                return Ok((await VerificationEngineService.GetDetails(entityType, entityId, workFlowStatusId, decodedFactString)));
            });
        }

        /// <summary>
        /// Gets the verification method.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{productId?}")]
        [ProducesResponseType(typeof(List<IFactResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetVerificationMethod(string entityType, string entityId, string productId)
        {
            return await ExecuteAsync(async () => Ok((await VerificationEngineService.GetFactVerification(entityType, entityId, productId))));
        }

        /// <summary>
        /// Gets the verification.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/do-not-load-methods/{productId?}")]         
        [ProducesResponseType(typeof(List<IFactResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetVerification(string entityType, string entityId, string productId)
        {
            return await ExecuteAsync(async () => Ok((await VerificationEngineService.GetFactVerification(entityType, entityId, productId, true))));
        }

        /// <summary>
        /// Gets the verification facts.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <returns></returns>
        [HttpGet("{productId}/{workFlowStatusId}/config-detail")]
        [ProducesResponseType(typeof(List<Configuration.IFact>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetVerificationFacts(string productId, string workFlowStatusId)
        {
            return await ExecuteAsync(async () => Ok(await VerificationEngineService.GetFactList(productId, workFlowStatusId)));
        }

        /// <summary>
        /// Initiates the manual verification.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="fact">The fact.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{workFlowStatusId}/{fact}/{methodName}/initiate-manualverification")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> InitiateManualVerification(string entityType, string entityId, string workFlowStatusId, string fact, string methodName)
        {
            return await ExecuteAsync(async () =>
            {
                var decodedFactString = WebUtility.UrlDecode(fact);
                var decodedMethodString = WebUtility.UrlDecode(methodName);
                await VerificationEngineService.InitiateManualVerification(entityType, entityId, workFlowStatusId, decodedFactString, decodedMethodString);
                return new NoContentResult();
            });
        }
        
        /// <summary>
        /// Initiates the syndication verification.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="fact">The fact.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{workFlowStatusId}/{fact}/{methodName}/initiate-syndication-verification")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> InitiateSyndicationVerification(string entityType, string entityId, string workFlowStatusId, string fact, string methodName)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var decodedFactString = WebUtility.UrlDecode(fact);
                    var decodedMethodString = WebUtility.UrlDecode(methodName);
                    await VerificationEngineService.InitiateSyndicationVerification(entityType, entityId, workFlowStatusId, decodedFactString, decodedMethodString);
                    return new NoContentResult();
                }
                catch (VerificationMethodInitiationFailed ex)
                {
                    return new ErrorResult(400, ex.Message);
                }

            });
        }

        /// <summary>
        /// Pendings the document.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{workFlowStatusId}/pending-document")]
        [ProducesResponseType(typeof(List<IPendingDocumentResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> PendingDocument(string entityType, string entityId, string workFlowStatusId)
        {
            return await ExecuteAsync(async () =>
                    Ok(await VerificationEngineService.GetPendingDocument(entityType, entityId, workFlowStatusId)));
        }

        /// <summary>
        /// Gets the required document.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="fact">The fact.</param>
        /// <returns></returns>
        [HttpGet("{productId}/{workFlowStatusId}/{fact}/required-document")]
        [ProducesResponseType(typeof(List<IPendingDocumentResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetRequiredDocument(string productId, string workFlowStatusId, string fact)
        {
            return await ExecuteAsync(async () =>
            {
                var decodedFactString = WebUtility.UrlDecode(fact);
                return Ok(await VerificationEngineService.GetRequiredDocument(productId, workFlowStatusId, decodedFactString));
            });
        }

        /// <summary>
        /// Gets the documents.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{workFlowStatusId}/documents")]
        [ProducesResponseType(typeof(List<IDocumentResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetDocuments(string entityType, string entityId, string workFlowStatusId)
        {
            return await ExecuteAsync(async () =>
                    Ok(await VerificationEngineService.GetDocuments(entityType, entityId, workFlowStatusId)));
        }

        /// <summary>
        /// Gets the documents by fact.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="factName">Name of the fact.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{workFlowStatusId}/{factName}/documentsDetails")]
        [ProducesResponseType(typeof(List<DocumentManager.IDocument>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetDocumentsByFact(string entityType, string entityId, string workFlowStatusId, string factName)
        {
            return await ExecuteAsync(async () =>
            {
                var decodedFactString = WebUtility.UrlDecode(factName);
                return Ok(await VerificationEngineService.GetDocumentsByFact(entityType, entityId, workFlowStatusId, decodedFactString));
            });
        }

        /// <summary>
        /// Verifies the document.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="factName">Name of the fact.</param>
        /// <param name="documentId">The document identifier.</param>
        /// <param name="status">The status.</param>
        /// <param name="reasons">The reasons.</param>
        /// <returns></returns>
        [HttpPut("{entityType}/{entityId}/{workFlowStatusId}/{factName}/{documentId}/{status}/{*reasons}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> VerifyDocument(string entityType, string entityId, string workFlowStatusId, string factName, string documentId, string status, string reasons)
        {

            return await ExecuteAsync(async () =>
            {
                var decodedFactString = WebUtility.UrlDecode(factName);
                await VerificationEngineService.VerifyDocument(entityType, entityId, workFlowStatusId, decodedFactString, documentId, status, SplitReasons(reasons));
                return NoContentResult;
            });
        }
        
        /// <summary>
        /// Resets the facts.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="resetFacts">The reset facts.</param>
        /// <returns></returns>
        [HttpPut("{entityType}/{entityId}/resetfacts")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ResetFacts(string entityType, string entityId, [FromBody] ResetFactRequest resetFacts) =>
          await ExecuteAsync(async () => { await VerificationEngineService.ReSetVerification(entityType, entityId, resetFacts); return NoContentResult; });

        /// <summary>
        /// Events the re publish.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{workFlowStatusId}/events-republish")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> EventRePublish(string entityType, string entityId, string workFlowStatusId)
        {
            return await ExecuteAsync(async () =>
            {
                await VerificationEngineService.EventRePublish(entityType, entityId, workFlowStatusId);
                return new NoContentResult();
            });
        }

        /// <summary>
        /// Initiates the dynamic fact verification.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="workFlowStatusId">The work flow status identifier.</param>
        /// <param name="dynamicFactRequest">The dynamic fact request.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{workFlowStatusId}/initiate-dynamicfact-verification")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> InitiateDynamicFactVerification(string entityType, string entityId, string workFlowStatusId, [FromBody] DynamicFactRequest dynamicFactRequest)
        {
            return await ExecuteAsync(async () =>
            {
                await VerificationEngineService.InitiateDynamicFactVerification(entityType, entityId, workFlowStatusId, dynamicFactRequest);
                return new NoContentResult();
            });
        }

        private static List<string> SplitReasons(string reasons)
        {
            if (string.IsNullOrWhiteSpace(reasons))
                return null;

            reasons = WebUtility.UrlDecode(reasons);

            return string.IsNullOrWhiteSpace(reasons)
                ? null
                : reasons.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
