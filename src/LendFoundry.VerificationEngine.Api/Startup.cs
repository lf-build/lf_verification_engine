﻿using LendFoundry.DataAttributes.Client;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.VerificationEngine.Persistance;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.Security.Encryption;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Voting.Client;
using LendFoundry.Clients.DecisionEngine;
using System;
using System.Runtime;
using LendFoundry.Configuration;
using System.Collections.Generic;

#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.VerificationEngine.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {

            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Verification Engine"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.VerificationEngine.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif

            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddEncryptionHandler();
            services.AddConfigurationService<VerificationConfiguration>(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddStatusManagementService();
            services.AddDocumentManager();
            services.AddIdentityService();
            services.AddDataAttributes();
            services.AddDecisionEngine();
            services.AddLookupService();
            services.AddProductRuleService();
            services.AddVoting();
            services.AddDependencyServiceUriResolver<VerificationConfiguration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);

            // internals
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<VerificationConfiguration>>().Get());
            services.AddTransient<IVerificationFactsRepository, VerificationFactsRepository>();
            services.AddTransient<IVerificationEngineService, VerificationEngineService>();
            services.AddTransient<IVerificationEngineListener, VerificationEngineListener>();
            services.AddTransient<IVerificationFactsRepositoryFactory, VerificationFactsRepositoryFactory>();
            services.AddTransient<IVerificationMethodRepository, VerificationMethodRepository>();
            services.AddTransient<IVerificationMethodHistoryRepository, VerificationMethodHistoryRepository>();
            services.AddTransient<IVerificationMethodHistoryRepositoryFactory, VerificationMethodHistoryRepositoryFactory>();
            services.AddTransient<IVerificationMethodRepositoryFactory, VerificationMethodRepositoryFactory>();
            services.AddTransient<IVerificationEngineServiceFactory, VerificationEngineServiceFactory>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Verification Engine");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseVerificationEngineListener();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
           }
    }
}
