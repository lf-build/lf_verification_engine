using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using LendFoundry.Security.Encryption;

namespace LendFoundry.VerificationEngine.Persistance
{
    public class VerificationMethodRepository : MongoRepository<IVerificationMethod, VerificationMethod>,
        IVerificationMethodRepository
    {
        #region Constructor
        static VerificationMethodRepository()
        {
            BsonClassMap.RegisterClassMap<VerificationMethod>(map =>
            {
                map.AutoMap();
                var type = typeof(VerificationMethod);
                // map.MapProperty(p => p.Documents).SetSerializer(new EnumSerializer<Document>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });           

            BsonClassMap.RegisterClassMap<Source>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.Type).SetSerializer(new EnumSerializer<SourceType>(BsonType.String));
                var type = typeof(Source);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<VerificationRule>(map =>
            {
                map.AutoMap();
                var type = typeof(VerificationRule);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Document>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.DocumentStatus).SetSerializer(new EnumSerializer<DocumentStatus>(BsonType.String));
                var type = typeof(Document);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public VerificationMethodRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "methods")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(VerificationSources)))
            {
                BsonClassMap.RegisterClassMap<VerificationSources>(map =>
                {
                    map.AutoMap();
                    var type = typeof(VerificationSources);
                    map.MapMember(m => m.DataExtracted).SetSerializer(new BsonEncryptor<object, object>(encrypterService));
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });

            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(MethodAttemptResult)))
            {
                BsonClassMap.RegisterClassMap<MethodAttemptResult>(map =>
                {
                    map.AutoMap();
                    var type = typeof(MethodAttemptResult);
                    map.MapProperty(p => p.Result).SetSerializer(new EnumSerializer<VerificationResult>(BsonType.String));
                    map.MapProperty(p => p.DataExtracted).SetSerializer(new BsonEncryptor<object, object>(encrypterService));
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            CreateIndexIfNotExists("tenant-id",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));

            CreateIndexIfNotExists("entityType-id-fact",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName));

            CreateIndexIfNotExists("method_id",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i=>i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName)
                    .Ascending(i => i.MethodName));

            CreateIndexIfNotExists("fact_id",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.FactName));

            CreateIndexIfNotExists("entityType-id",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId));


            CreateIndexIfNotExists("entityType-id-product",
                Builders<IVerificationMethod>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId)
                    .Ascending(i => i.ProductId));
        }
        #endregion

        #region Public Method
        public async Task<IVerificationMethod> Get(string entityType, string entityId, string factName, string methodName)
        {
            return await Query.FirstOrDefaultAsync(
                fact => fact.EntityType == entityType &&
                        fact.EntityId == entityId &&
                        fact.FactName == factName &&
                        fact.MethodName == methodName);
        }

        public async Task<List<IVerificationMethod>> Get(string entityType, string entityId, string factName)
        {
            return await Query.Where(
                    fact => fact.EntityType == entityType &&
                            fact.EntityId == entityId &&
                            fact.FactName == factName
                ).ToListAsync();
        }

        public async Task<IVerificationMethod> AddSource(string entityType, string entityId,string productId, string statusWorkFlowId,string factName, string methodName, IVerificationSources verificationSource)
        {
            var verificationMethod = await Get(entityType, entityId, factName, methodName);
            if (verificationMethod == null)
            {
                verificationMethod = new VerificationMethod()
                {
                    EntityType = entityType,
                    EntityId = entityId,
                    ProductId = productId,
                    StatusWorkFlowId = statusWorkFlowId,
                    VerificationSources = new List<IVerificationSources> { verificationSource },
                    MethodName = methodName,
                    FactName = factName
                };

                Add(verificationMethod);
            }
            else
            {
                var currentSources = verificationMethod.VerificationSources;
                if (currentSources == null)
                {
                    currentSources = new List<IVerificationSources> { verificationSource };
                }
                else
                {
                    currentSources.Add(verificationSource);
                }
                verificationMethod.VerificationSources = currentSources;

                await Collection.UpdateOneAsync(Builders<IVerificationMethod>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                    a.EntityType == entityType && a.EntityId == entityId && a.FactName == factName && a.MethodName == methodName),
                Builders<IVerificationMethod>.Update.Set(a => a.VerificationSources, currentSources));
            }
            return verificationMethod;
        }


        public async Task<IVerificationMethod> ResetMethod( IVerificationMethod verificationMethod)
        {
            await Collection.UpdateOneAsync(Builders<IVerificationMethod>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                  a.EntityType == verificationMethod.EntityType && a.EntityId == verificationMethod.EntityId && a.FactName == verificationMethod.FactName && a.MethodName == verificationMethod.MethodName),
          Builders<IVerificationMethod>.Update.Set(a => a.VerificationSources, null)
            .Set(a => a.Documents, null)
           .Set(a => a.Attempts, null));
     
            return verificationMethod;
        }

        public async Task<IVerificationMethod> AddAttempt(string id, IMethodAttemptResult attemptResult)
        {
            var verificationMethod = await Get(id);

            if (verificationMethod.Attempts == null)
                verificationMethod.Attempts = new List<IMethodAttemptResult> { attemptResult };
            else
                verificationMethod.Attempts.Add(attemptResult);

            await Collection.UpdateOneAsync(Builders<IVerificationMethod>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.Id == id),
               Builders<IVerificationMethod>.Update.Set(a => a.Attempts, verificationMethod.Attempts));

            return verificationMethod;
        }

        public async Task<IVerificationMethod> AddDocuments(string entityType, string entityId, string productId, string statusWorkFlowId, string factName, string methodName, Document document)
        {
            var verificationMethod = await Get(entityType, entityId, factName, methodName);
            if (verificationMethod == null)
            {
                List<Document> documents = new List<Document>();
                documents.Add(document);
                verificationMethod = new VerificationMethod
                {
                    EntityType = entityType,
                    EntityId = entityId,
                    ProductId = productId,
                    StatusWorkFlowId = statusWorkFlowId,
                    Documents = documents,
                    MethodName = methodName,
                    FactName = factName

                };

                Add(verificationMethod);
            }
            else
            {
                if (verificationMethod.Documents != null)
                    verificationMethod.Documents.Add(document);
                else
                {
                    List<Document> documents = new List<Document>();
                    documents.Add(document);
                    verificationMethod.Documents = documents;
                }
                Update(verificationMethod);

            }


            return verificationMethod;
        }
        
        public async Task<List<IVerificationMethod>> GetMethodDetails(string entityType, string entityId,string productId)
        {
            if(!string.IsNullOrEmpty(productId))
            return await
                Query.Where(
                    fact => fact.EntityType == entityType &&
                            fact.EntityId == entityId && fact.ProductId == productId
                ).ToListAsync();
            else              
                return await
                    Query.Where(
                        fact => fact.EntityType == entityType &&
                                fact.EntityId == entityId 
                    ).ToListAsync();
        }

        public async Task<List<IVerificationMethod>> GetPendingDocuments(string entityType, string entityId, DocumentStatus status)
        {
            return await Query.Where(method => method.EntityType == entityType && method.EntityId == entityId).ToListAsync();
        }

        #endregion
    }
}