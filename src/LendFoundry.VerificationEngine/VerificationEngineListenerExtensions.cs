﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

#endif


namespace LendFoundry.VerificationEngine
{
    public static class VerificationEngineListenerExtensions
    {
        public static void UseVerificationEngineListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<IVerificationEngineListener>().Start();
        }
    }
}
