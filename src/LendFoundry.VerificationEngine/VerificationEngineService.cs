﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductRule;
using LendFoundry.Security.Tokens;
using LendFoundry.VerificationEngine.Configuration;
using Microsoft.CSharp.RuntimeBinder;
using SourceType = LendFoundry.VerificationEngine.Configuration.SourceType;
using LendFoundry.DocumentManager;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;
using LendFoundry.Security.Identity.Client;
using LendFoundry.StatusManagement;
using LendFoundry.Voting;
using System.IO;

namespace LendFoundry.VerificationEngine
{
    public class VerificationEngineService : IVerificationEngineServiceInternal
    {
        #region Constructor

        public VerificationEngineService
            (
                IVerificationFactsRepository verificationFactRepository,
                IEventHubClient eventHubClient,
                ITenantTime tenantTime,
                IDocumentManagerService documentManagerService,
                VerificationConfiguration configuration,
                IProductRuleService productRuleService,
                ILookupService lookupService,
                IIdentityService identityService,
                ITokenReader tokenReader,
                ITokenHandler tokenParser,
                IVerificationMethodRepository verificationMethodRepository,
                IDataAttributesEngine dataAttributeEngine,
                IVerificationMethodHistoryRepository verificationMethodHistoryRepository,
                IVotingService votingservice,
                IDecisionEngineService decisionEngineService,
                IEntityStatusService entityStatusService
            )
        {
            TenantTime = tenantTime;
            VerificationFactRepository = verificationFactRepository;
            EventHubClient = eventHubClient;
            DocumentManagerService = documentManagerService;
            Configuration = configuration;
            ProductRuleService = productRuleService;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            VerificationMethodRepository = verificationMethodRepository;
            LookupService = lookupService;
            DataAttributeEngine = dataAttributeEngine;
            IdentityService = identityService;
            VerificationMethodHistoryRepository = verificationMethodHistoryRepository;
            VotingService = votingservice;
            DecisionEngineService = decisionEngineService;
            EntityStatusService = entityStatusService;
        }

        #endregion

        #region Private Properties
        private IVerificationMethodHistoryRepository VerificationMethodHistoryRepository { get; }
        private IVotingService VotingService { get; }
        private IIdentityService IdentityService { get; }
        private ILookupService LookupService { get; }
        private ITenantTime TenantTime { get; }
        private IVerificationFactsRepository VerificationFactRepository { get; }
        private IVerificationMethodRepository VerificationMethodRepository { get; }
        private VerificationConfiguration Configuration { get; }
        private IEventHubClient EventHubClient { get; }
        private IProductRuleService ProductRuleService { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IDocumentManagerService DocumentManagerService { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }
        private IDataAttributesEngine DataAttributeEngine { get; }
        private IEntityStatusService EntityStatusService { get; }

        #endregion

        #region Public Method

        public async Task<IVerificationMethod> Verify(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, string sourceName, object request)
        {
            return await Verify(entityType, entityId, workFlowStatusId, factName, methodName, sourceName, true, null, request);
        }

        public async Task<IVerificationMethod> Verify(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, string sourceName, bool isManual, string productId, object input)
        {
            entityType = EnsureEntityType(entityType);
            if (productId == null)
                productId = await GetProductId(entityType, entityId);

            var currentUser = EnsureCurrentUser();
            IFact fact;
            IMethod method;
            object attributes = null;
            VerificationFacts verificationFact;
            IVerificationMethod verificationMethod;
            MethodAttemptResult latestMethodAttempt = null;

            var dynamicFacts = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (dynamicFacts != null && dynamicFacts.FactType == FactType.Dynamic)
            {
                fact = new Fact { Name = dynamicFacts.FactName };
                method = new Method { Name = dynamicFacts.CurrentInitiatedMethod };

                var pendingDocuments = await GetPendingDocument(entityType, entityId, workFlowStatusId);
                var sourceDocumentPending = pendingDocuments.FirstOrDefault(i => i.MethodName == methodName);
                if (sourceDocumentPending != null)
                    throw new InvalidOperationException("Document is Pending");

                verificationMethod = await VerificationMethodRepository.AddSource(entityType, entityId, productId, workFlowStatusId, fact.Name, method.Name,
                    new VerificationSources
                    {
                        DataExtractedDate = new TimeBucket(TenantTime.Now),
                        SourceName = sourceName,
                        DataExtracted = new { input },
                        Source = null
                    });

                var verificationResult = GetVerificationResult(input, true);
                latestMethodAttempt = new MethodAttemptResult
                {
                    DateAttempt = new TimeBucket(TenantTime.Now),
                    VerifiedBy = currentUser,
                    Result = verificationResult,
                    SourceName = sourceName
                };

                verificationMethod = await VerificationMethodRepository.AddAttempt(verificationMethod.Id, latestMethodAttempt);

                verificationFact = new VerificationFacts
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ProductId = productId,
                    StatusWorkFlowId = workFlowStatusId,
                    FactName = fact.Name,
                    LastVerifiedBy = currentUser,
                    VerificationStatus = verificationResult == VerificationResult.Passed ? VerificationStatus.Passed : VerificationStatus.Failed,
                    LastVerifiedDate = new TimeBucket(TenantTime.Now),
                    CurrentStatus = verificationResult == VerificationResult.Passed ? CurrentStatus.Completed : CurrentStatus.InProgress,
                    CurrentInitiatedMethod = method.Name

                };
                await VerificationFactRepository.AddOrUpdate(verificationFact);
                verificationFact.RequestType = "Form";
            }
            else
            {
                fact = EnsureFact(workFlowStatusId, factName, productId);
                method = EnsureFactMethod(fact, methodName);
                var source = method.Sources.FirstOrDefault(s => string.Equals(s.Name, sourceName, StringComparison.CurrentCultureIgnoreCase));

                if (source == null)
                    throw new NotFoundException($"Source {sourceName} not found");

                if (method.RolesForAccess != null && method.RolesForAccess.Any())
                {
                    var currentUserRoles = await IdentityService.GetUserRoles(currentUser);
                    if (currentUserRoles != null)
                    {
                        var rolesExists = currentUserRoles.ToList().Intersect(method.RolesForAccess);
                        if (!rolesExists.Any())
                            throw new InvalidOperationException($"You don't have permission to verify {fact.Name} fact.");
                    }
                }

                // If Document Upload is pending then user should not be able to upload document.
                if (source.Type == SourceType.Form || source.Type == SourceType.Review)
                {
                    var pendingDocuments = await GetPendingDocument(entityType, entityId, workFlowStatusId);
                    var sourceDocumentPending = pendingDocuments?.Where(i => i.MethodName == methodName).FirstOrDefault();
                    if (sourceDocumentPending != null)
                        throw new InvalidOperationException("Document is Pending");
                }
                if (source.SaveToDataAttribute != null)
                {
                    await DataAttributeEngine.SetAttribute(entityType, entityId, source.SaveToDataAttribute, input);
                }

                if (source.RequiredDataAttributes != null && source.RequiredDataAttributes.Count > 0)
                    attributes = await DataAttributeEngine.GetAttribute(entityType, entityId, source.RequiredDataAttributes);

                verificationMethod = await VerificationMethodRepository.AddSource(entityType, entityId, productId, workFlowStatusId, fact.Name, method.Name,
                    new VerificationSources
                    {
                        DataExtractedDate = new TimeBucket(TenantTime.Now),
                        SourceName = sourceName,
                        DataExtracted = new { input, attributes },
                        Source = source
                    });

                var verificationResult = VerificationResult.UnableToSpecify;
                string reason = null;
                object interMediateData = null;
                if (CanExecuteRule(method, verificationMethod))
                {
                    if (source.Type == SourceType.Automatic)
                    {
                        verificationResult = GetVerificationResult(input);
                    }
                    else
                    {
                        if (Configuration.UseDecisionEngine)
                        {
                            var extractedDataAllSource = ExtractedDataSources(verificationMethod);
                            var result = ExecuteRule(extractedDataAllSource, method, entityId);
                            verificationResult = result.Item1;
                            reason = result.Item2;
                            interMediateData = result.Item3;
                        }
                        else
                        {
                            verificationResult = await ExecuteUsingProductRule(fact, method, entityId, entityType, productId);
                        }
                    }
                    if (verificationResult == VerificationResult.None)
                    {
                        return verificationMethod;
                    }
                    latestMethodAttempt = new MethodAttemptResult
                    {
                        DateAttempt = new TimeBucket(TenantTime.Now),
                        VerifiedBy = currentUser,
                        Result = verificationResult,
                        SourceName = sourceName,
                        Reason = reason,
                        DataExtracted = interMediateData
                    };
                    verificationMethod = await VerificationMethodRepository.AddAttempt(verificationMethod.Id, latestMethodAttempt);
                }

                if (verificationResult == VerificationResult.UnableToSpecify)
                {
                    await InitiateReviewMthodOnFailure(entityType, entityId, fact, method, workFlowStatusId);
                    throw new InvalidOperationException("Unable To Verify Fact");
                }

                verificationFact = new VerificationFacts
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ProductId = productId,
                    StatusWorkFlowId = workFlowStatusId,
                    FactName = fact.Name,
                    LastVerifiedBy = currentUser,
                    VerificationStatus = VerificationStatus.None,
                    LastVerifiedDate = new TimeBucket(TenantTime.Now),
                    CurrentStatus = CurrentStatus.InProgress,
                    CurrentInitiatedMethod = method.Name

                };
                if (verificationMethod.Attempts == null || !verificationMethod.Attempts.Any())
                {
                    verificationFact.CurrentStatus = CurrentStatus.InProgress;
                    verificationFact.VerificationStatus = VerificationStatus.None;
                    await VerificationFactRepository.AddOrUpdate(verificationFact);
                    return verificationMethod;
                }
                if (verificationMethod.Attempts.Any(r => r.Result == VerificationResult.Passed))
                {
                    verificationFact.CurrentStatus = CurrentStatus.Completed;
                    verificationFact.VerificationStatus = VerificationStatus.Passed;
                    verificationFact.MethodName = verificationMethod.Attempts.Where(i => i.Result == VerificationResult.Passed).OrderByDescending(i => i.DateAttempt.Time).FirstOrDefault()?.SourceName;
                    var sourceNameDetail = verificationMethod.VerificationSources.Where(i => i.SourceName == verificationFact.MethodName).OrderByDescending(i => i.DataExtractedDate.Time).FirstOrDefault()?.Source.Type.ToString();
                    verificationFact.RequestType = (sourceNameDetail == "Form" ? "Manual" : sourceNameDetail);
                }
                else if (verificationResult == VerificationResult.Errored)
                {
                    verificationFact.CurrentStatus = CurrentStatus.Errored;
                    verificationFact.VerificationStatus = VerificationStatus.Errored;
                    verificationFact.MethodName = sourceName;
                    verificationFact.RequestType = (source.Type.ToString() == "Form" ? "Manual" : source.Type.ToString());
                }
                else
                {
                    verificationFact.CurrentStatus = CurrentStatus.InProgress;
                    verificationFact.VerificationStatus = VerificationStatus.Failed;
                    verificationFact.MethodName = sourceName;
                    verificationFact.RequestType = (source.Type.ToString() == "Form" ? "Manual" : source.Type.ToString());
                }
                await VerificationFactRepository.AddOrUpdate(verificationFact);

                // if fact is falied and review method is configured for the same , Initiate it .
                if ((verificationFact.CurrentStatus == CurrentStatus.InProgress) && (verificationFact.VerificationStatus == VerificationStatus.Failed))
                    await InitiateReviewMthodOnFailure(entityType, entityId, fact, method, workFlowStatusId);

            }

            await EventHubClient.Publish(new FactVerificationCompleted(verificationFact, latestMethodAttempt));
            return verificationMethod;
        }

        public async Task<string> GetProductId(string entityType, string entityId)
        {
            var applicationAttributes = await DataAttributeEngine.GetAttribute(entityType, entityId, "product");
            if (applicationAttributes == null)
                throw new NotFoundException($"Product data-attribute is not found for {entityType} - {entityId}");

            var productId = GetResultValueForProductId(applicationAttributes);

            return productId;
        }

        public async Task<IVerificationFacts> GetFactDetailsByPollingId(string entityType, string entityId, string productId, string pollingId)
        {
            return await VerificationFactRepository.GetFactDetailsByPollingId(entityType, entityId, productId, pollingId);

        }

        private async Task InitiateReviewMthodOnFailure(string entityType, string entityId, IFact fact, IMethod method, string workFlowStatusId)
        {
            if (method.InitiateReviewonFailure)
            {
                var reviewMethod = fact.Methods.FirstOrDefault(i => i.IsReviewMethod);
                if (reviewMethod != null)
                {
                    await InitiateManualVerification(entityType, entityId, workFlowStatusId, fact.Name, reviewMethod.Name);
                }
            }
        }

        private static VerificationResult GetVerificationResult(object input, bool isDynamic = false)
        {
            VerificationResult verificationResult;
            if (input != null)
            {
                var result = isDynamic ? GetResultValueForDynamic(input) : GetResultValue(input);
                if (result == null)
                    throw new InvalidArgumentException("Request data is not in correct format");
                verificationResult = result.ToLower() == "true" ?
                    VerificationResult.Passed :
                    VerificationResult.Failed;
            }
            else
            {
                verificationResult = VerificationResult.UnableToSpecify;
            }

            return verificationResult;
        }

        public async Task<IVerificationFacts> GetStatus(string entityType, string entityId, string workFlowStatusId, string factName)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var productId = await GetProductId(entityType, entityId);
            var verificationFact = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (verificationFact != null && verificationFact.FactType == FactType.Dynamic) { }
            else
            {
                EnsureFact(workFlowStatusId, factName, productId);
            }
            if (string.IsNullOrEmpty(verificationFact?.Id))
                throw new NotFoundException($"{factName} is not initiated for {entityId}");
            return verificationFact;
        }

        public async Task ReSetVerification(string entityType, string entityId, IResetFactRequest resetFacts)
        {

            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            if (resetFacts == null || resetFacts.FactNames.Count <= 0)
                throw new ArgumentException("resetFacts is required");

            var productId = await GetProductId(entityType, entityId);
            var currentWorkFlow = await EntityStatusService.GetActiveStatusWorkFlow(entityType, entityId);

            foreach (var item in resetFacts.FactNames)
            {
                var fact = EnsureFact(currentWorkFlow.StatusWorkFlowId, item, productId);
                var verificationFact = await VerificationFactRepository.Get(entityType, entityId, fact.Name);

                if (verificationFact != null)
                {
                    verificationFact.CurrentInitiatedMethod = null;
                    verificationFact.CurrentStatus = CurrentStatus.NotInitiated;
                    verificationFact.LastVerifiedDate = null;
                    verificationFact.LastVerifiedBy = null;
                    verificationFact.VerificationStatus = VerificationStatus.None;
                    VerificationFactRepository.Update(verificationFact);
                }

                var verificationMethods = await VerificationMethodRepository.Get(entityType, entityId, fact.Name);

                if (verificationMethods != null)
                {
                    foreach (var method in verificationMethods)
                    {
                        VerificationMethodHistoryRepository.Add(method);

                        method.Attempts = null;
                        method.Documents = null;
                        method.VerificationSources = null;
                        await VerificationMethodRepository.ResetMethod(method);
                    }
                }

            }

        }

        public async Task<List<IVerificationMethod>> GetDetails(string entityType, string entityId, string workFlowStatusId, string factName)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var productId = await GetProductId(entityType, entityId);

            IFact fact;
            var dynamicFacts = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (dynamicFacts != null && dynamicFacts.FactType == FactType.Dynamic)
            {
                fact = new Fact { Name = dynamicFacts.FactName };
            }
            else
            {
                fact = EnsureFact(workFlowStatusId, factName, productId);
            }

            var verificationDetails = await VerificationMethodRepository.Get(entityType, entityId, fact.Name);
            if (!verificationDetails.Any())
            {
                throw new NotFoundException($"{factName} is not initiated for {entityId}");
            }
            return verificationDetails;
        }

        public async Task<List<LendFoundry.DocumentManager.IDocument>> GetDocumentsByFact(string entityType, string entityId, string workFlowStatusId, string factName)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var productId = await GetProductId(entityType, entityId);

            IFact fact;
            var dynamicFacts = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (dynamicFacts != null && dynamicFacts.FactType == FactType.Dynamic)
            {
                fact = new Fact { Name = dynamicFacts.FactName };
            }
            else
            {
                fact = EnsureFact(workFlowStatusId, factName, productId);
            }

            var verificationDetails = await VerificationMethodRepository.Get(entityType, entityId, fact.Name);
            if (!verificationDetails.Any())
            {
                throw new NotFoundException($"There are no records for {factName} and {entityId}");
            }

            var documentsList = await DocumentManagerService.GetAll(entityType, entityId);
            if (documentsList == null || !documentsList.Any())
            {
                throw new NotFoundException($"There are no records for {factName} and {entityId} in application Documents");
            }
            var documents = new List<LendFoundry.DocumentManager.IDocument>();
            foreach( var document in documentsList)
            {
                var metaDataFact = GetFactValue(document.Metadata);
                var documentTags = new List<string>();
                foreach (var method in fact.Methods)
                {
                    if(method.DocumentTags != null && method.DocumentTags.Any())
                        documentTags.AddRange(method.DocumentTags);
                }
                if(metaDataFact == factName || documentTags.Any(t => document.Tags.Contains(t)))
                {
                    documents.Add(document);
                }
            }
            return documents;
        }

        public async Task VerifyDocument(string entityType, string entityId, string workFlowStatusId, string factName, string documentId, string statusCode, List<string> reasons = null)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var productId = await GetProductId(entityType, entityId);

            IFact fact;
            var dynamicFacts = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (dynamicFacts != null && dynamicFacts.FactType == FactType.Dynamic)
            {
                fact = new Fact { Name = dynamicFacts.FactName };
            }
            else
            {
                fact = EnsureFact(workFlowStatusId, factName, productId);
            }

            var verificationDetails = await VerificationMethodRepository.Get(entityType, entityId, fact.Name);
            if (!verificationDetails.Any())
            {
                throw new NotFoundException($"There are no records for {factName} and {entityId}");
            }
            var content = (DocumentStatus)Enum.Parse(typeof(DocumentStatus), statusCode);
            foreach (var item in verificationDetails)
            {
                if (item.Documents != null && item.Documents.Count > 0)
                {
                    var isDocumentExists = item.Documents.FirstOrDefault(i => i.DocumentGeneratedId == documentId);
                    if (isDocumentExists != null)
                    {
                        isDocumentExists.DocumentStatus = content;
                        isDocumentExists.Reasons = reasons;
                        VerificationMethodRepository.Update(item);
                        if (statusCode == DocumentStatus.Unverifiable.ToString())
                        {
                            await UpdateFactDetails(entityType, entityId, factName);
                        }
                        await DocumentManagerService.VerifyDocument(entityType, entityId, documentId, statusCode);
                        return;
                    }
                }
            }
        }

        private async Task UpdateFactDetails(string entityType, string entityId, string factName)
        {
            var factDetails = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (factDetails != null)
            {
                factDetails.CurrentStatus = CurrentStatus.Initiated;
                factDetails.VerificationStatus = VerificationStatus.None;
                VerificationFactRepository.Update(factDetails);
            }
        }

        public async Task<List<IFact>> GetFactList(string productId, string workFlowStatusId)
        {

            return await Task.Run(() =>
            {
                var facts = EnsureFacts(productId, workFlowStatusId).ToList();
                return new List<IFact>(facts);
            });
        }

        public async Task InitiateManualVerification(string entityType, string entityId, string workFlowStatusId, string factName, string currentInitiateMethod)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entityId is required.");

            entityType = EnsureEntityType(entityType);

            var productId = await GetProductId(entityType, entityId);

            var dynamicFacts = await VerificationFactRepository.Get(entityType, entityId, factName);
            Fact fact;
            IMethod method;
            if (dynamicFacts != null && dynamicFacts.FactType == FactType.Dynamic)
            {
                fact = new Fact { Name = dynamicFacts.FactName };
                method = new Method { Name = dynamicFacts.CurrentInitiatedMethod };
            }
            else
            {
                fact = EnsureFact(workFlowStatusId, factName, productId);
                method = EnsureFactMethod(fact, currentInitiateMethod);
            }

            var currentUser = EnsureCurrentUser();
            if (fact.IsOptional)
            {
                if (!string.IsNullOrEmpty(fact.OptionalFactGroupName))
                {
                    //var secondaryResult = await SecondaryKeyCheck(entityId, entityType, fact, productId);
                    var result = await ProductRuleService.RunRule(entityType, entityId, productId, fact.OptionalFactGroupName);

                    if (result == null)
                        throw new VerificationFailedException("Unable to execute rule : " + fact.OptionalFactGroupName);
                    var ruleExecutionResult = result;

                    if (ruleExecutionResult.Result == Result.Failed)
                        throw new InvalidOperationException("Optional fact can not be initiated");

                }
            }

            // When the Poll is attached with any Fact it will be automatically created and Initiated with details of the Configurations.
            IPoll pollDetails = null;
            if (!string.IsNullOrEmpty(method.Poll?.PollName))
            {
                IPoll poll = new Poll();
                poll.entityId = entityId;
                poll.entityType = entityType;
                poll.options = method.Poll.options.ToList<IOption>();
                poll.description = method.Poll.Description;
                poll.subject = method.Poll.PollName;
                poll.participants = method.Poll.participants.ToList<IParticipant>();
                poll.closesOn = TenantTime.Now.AddDays(Convert.ToDouble(method.Poll.CloseOnDays)).ToString("yyyy-MM-dd");
                pollDetails = await VotingService.createPoll(poll);
                if (pollDetails != null)
                    await VotingService.updatePollStatus(pollDetails._id, "initiated");
            }

            var uploadedDocuments = await VerificationMethodRepository.Get(entityType, entityId, fact.Name, currentInitiateMethod);
            if (uploadedDocuments?.Documents != null && uploadedDocuments.Documents.Count > 0)
            {
                var verifiableDocuments = uploadedDocuments.Documents.FirstOrDefault(i => i.DocumentStatus == DocumentStatus.Verifiable);
                if (verifiableDocuments != null)
                    return;
            }

            await ChangeFactStatus(entityType, entityId, productId, fact.Name, currentUser, CurrentStatus.Initiated, method.Name, workFlowStatusId, pollDetails?._id);
            await EventHubClient.Publish(new ManualFactVerificationInitiated(entityType, entityId, fact.Name));
        }

        public async Task InitiateSyndicationVerification(string entityType, string entityId, string workFlowStatusId, string factName, string currentInitiateMethod)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entityId is required.");

            entityType = EnsureEntityType(entityType);
            string productId = null;
            FaultRetry.RunWithAlwaysRetry(() =>
            {
                productId = GetProductId(entityType, entityId).Result;
                if (string.IsNullOrWhiteSpace(productId))
                    throw new NotFoundException("Product Not found:" + productId);
            }, 3, 10);

            var fact = EnsureFact(workFlowStatusId, factName, productId);
            var method = EnsureFactMethod(fact, currentInitiateMethod);

            var currentUser = EnsureCurrentUser();

            var methods = await VerificationMethodRepository.GetMethodDetails(entityType, entityId, productId);
            var methodFromDb = methods.FirstOrDefault(m => m.MethodName == method.Name && m.FactName == fact.Name);
            var anyPassed = methodFromDb?.Attempts.FirstOrDefault(m => m.Result == VerificationResult.Passed);
            if (anyPassed != null)
            {
                return;
            }

            foreach (var source in method.Sources)
            {
                if (source.Type != SourceType.Syndication || string.IsNullOrWhiteSpace(source.InitiationRule)) continue;
                try
                {
                    dynamic ruleResult = null;
                    FaultRetry.RunWithAlwaysRetry(() =>
                    {
                        ruleResult = DecisionEngineService.Execute<dynamic>(source.InitiationRule, new { entityType, entityId });
                        if (ruleResult == null)
                            throw new ArgumentException("Rule result is invalid");
                    }, source.MaxRetryForSyndicationRule, source.DurationOfRetryForSyndication);

                    dynamic dataExtracted = null;
                    if (!string.IsNullOrWhiteSpace(source.ExtractRule))
                    {
                        dataExtracted = DecisionEngineService.Execute<dynamic>(source.ExtractRule, ruleResult);
                    }

                    await ChangeFactStatus(entityType, entityId, productId, fact.Name, currentUser, CurrentStatus.Initiated, method.Name, workFlowStatusId);

                    if (!string.IsNullOrWhiteSpace(source.ExtractRule))
                    {
                        await Verify(entityType, entityId, workFlowStatusId, fact.Name, method.Name, source.Name, dataExtracted);
                    }

                    await EventHubClient.Publish(new ManualFactVerificationInitiated(entityType, entityId, fact.Name));
                }
                catch (Exception)
                {
                    await ChangeFactStatus(entityType, entityId, productId, fact.Name, currentUser, CurrentStatus.Errored, method.Name, workFlowStatusId);
                    throw;
                }
            }
        }

        public async Task<LendFoundry.DocumentManager.IDocument> AddDocumentDetails(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, LendFoundry.DocumentManager.IDocument documentInfo)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entityId is required");

            entityType = EnsureEntityType(entityType);
            var productId = await GetProductId(entityType, entityId);
            var currentUser = EnsureCurrentUser();
            IFact fact;
            IMethod method;
            string categoryName;
            string documentName;
            var dynamicFacts = await VerificationFactRepository.Get(entityType, entityId, factName);
            if (dynamicFacts != null && dynamicFacts.FactType == FactType.Dynamic)
            {
                fact = new Fact { Name = dynamicFacts.FactName };
                categoryName = dynamicFacts.DocumentDetails.Select(d => d.Category).FirstOrDefault();
                documentName = dynamicFacts.DocumentDetails.Select(d => d.Name).FirstOrDefault();
                method = new Method { Name = dynamicFacts.CurrentInitiatedMethod };
            }
            else
            {
                fact = EnsureFact(workFlowStatusId, factName, productId);
                method = EnsureFactMethod(fact, methodName);
                categoryName = method.Sources.FirstOrDefault()?.Settings["Category"];
                documentName = method.Sources.FirstOrDefault()?.Settings["DocumentRequired"];
            }
            
            if (string.IsNullOrEmpty(documentInfo?.DocumentId))
                throw new ArgumentException("Document is not uploaded successfully");

            await ChangeFactStatus(entityType, entityId, productId, fact.Name, currentUser, CurrentStatus.InProgress, methodName, workFlowStatusId);

            var document = new Document
            {
                DocumentGeneratedId = documentInfo.DocumentId,
                DocumentStatus = DocumentStatus.Verifiable
            };
            await VerificationMethodRepository.AddDocuments(entityType, entityId, productId, workFlowStatusId, fact.Name, method.Name, document);

            var pendingDocuments = await GetPendingDocument(entityType, entityId, workFlowStatusId);

            await EventHubClient.Publish(new VerificationDocumentReceived(entityType, entityId, fact.Name, pendingDocuments.Count, currentUser, documentName));
            return documentInfo;
        }

        public async Task<List<IPendingDocumentResponse>> GetPendingDocument(string entityType, string entityId, string workFlowStatusId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var items = new List<IPendingDocumentResponse>();
            var pendingFacts = await VerificationFactRepository.Get(entityType, entityId, CurrentStatus.Initiated);
            var productId = await GetProductId(entityType, entityId);
            foreach (var item in pendingFacts)
            {
                if (item.FactType != FactType.Dynamic)
                {
                    var configFact = EnsureFact(workFlowStatusId, item.FactName, productId);

                    if (configFact == null)
                        throw new NotFoundException($"No verification facts are found for {entityType}");

                    if (configFact.Methods == null || !configFact.Methods.Any())
                        return null;

                    var currentMethodInitiated = configFact.Methods.FirstOrDefault(i => i.Name == item.CurrentInitiatedMethod);

                    if (currentMethodInitiated != null)
                    {
                        foreach (var methodSource in currentMethodInitiated.Sources)
                        {
                            var documents = new List<string>();
                            if (methodSource.Type == SourceType.Form || methodSource.Type == SourceType.Review)
                            {
                                string document;
                                if (methodSource.Settings.TryGetValue("DocumentRequired", out document))
                                {
                                    documents.Add(document);
                                }
                                if (document != null)
                                    items.Add(new PendingDocumentResponse { FactType = item.FactType, FactName = configFact.Name, MethodName = currentMethodInitiated.Name, DocumentName = documents });
                            }
                        }
                    }
                }
                else
                {
                    if (item.DocumentDetails != null && item.DocumentDetails.Any())
                    {
                        items.Add(new PendingDocumentResponse { FactType = item.FactType, FactName = item.FactName, MethodName = item.CurrentInitiatedMethod, DocumentName = item.DocumentDetails.Select(d => d.Name).ToList() });
                    }
                }
            }
            return items;
        }

        public async Task<List<IFactResponse>> GetFactVerification(string entityType, string entityId, string productId = null, bool doNotLoadMethods = false)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Entity id is required");

            var facts = await VerificationFactRepository.GetFactDetails(entityType, entityId, productId);
            List<IVerificationMethod> methods = null;
            if (doNotLoadMethods == false)
            {
                methods = await VerificationMethodRepository.GetMethodDetails(entityType, entityId, productId);
            }
            List<IFactResponse> factResponse = null;
            if (facts != null)
            {
                factResponse = new List<IFactResponse>();
                foreach (var fact in facts)
                {
                    var response = new FactResponse
                    {
                        EntityType = entityType,
                        EntityId = entityId,
                        CurrentStatus = fact.CurrentStatus,
                        FactName = fact.FactName,
                        LastVerifiedBy = fact.LastVerifiedBy,
                        LastVerifiedDate = fact.LastVerifiedDate,
                        VerificationStatus = fact.VerificationStatus,
                        CurrentInitiatedMethod = fact.CurrentInitiatedMethod,
                        ProductId = fact.ProductId,
                        StatusWorkFlowId = fact.StatusWorkFlowId,
                        IsDynamic = (fact.FactType == FactType.Dynamic)
                    };

                    if (methods != null && methods.Any())
                    {
                        var factMethods = methods.Where(m => m.FactName.ToLower() == fact.FactName.ToLower()).ToList();
                        if (factMethods.Any())
                        {
                            foreach (var method in factMethods)
                            {
                                if (method.Attempts != null && method.Attempts.Count > 0)
                                {
                                    response.Attempts.AddRange(method.Attempts);
                                }
                                if (method.VerificationSources != null && method.VerificationSources.Count > 0)
                                {
                                    response.VerificationSources.AddRange(method.VerificationSources);
                                }
                            }
                        }
                    }
                    factResponse.Add(response);
                }
            }
            return factResponse;
        }

        public async Task<List<IRequiredDocumentResponse>> GetRequiredDocument(string productId, string workFlowStatusId, string factName)
        {
            var items = new List<IRequiredDocumentResponse>();
            await Task.Run(() =>
            {
                var configFact = EnsureFact(workFlowStatusId, factName, productId);

                if (configFact == null)
                    throw new NotFoundException($"No verification facts are found for {productId}");

                if (configFact.Methods == null || !configFact.Methods.Any())
                    return;

                foreach (var method in configFact.Methods)
                {
                    foreach (var methodSource in method.Sources)
                    {
                        var documents = new List<string>();
                        if (methodSource.Type == SourceType.Form || methodSource.Type == SourceType.Review)
                        {
                            string document;
                            if (methodSource.Settings.TryGetValue("DocumentRequired", out document))
                            {
                                documents.Add(document);
                            }
                            items.Add(new RequiredDocumentResponse { MethodName = method.Name, Documents = documents });
                        }
                    }
                }
            });
            return items;
        }

        public async Task<List<IDocumentResponse>> GetDocuments(string entityType, string entityId, string workFlowStatusId)
        {
            var items = new List<IDocumentResponse>();
            var pendingDocuments = await GetPendingDocument(entityType, entityId, workFlowStatusId);
            var productId = await GetProductId(entityType, entityId);
            await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);

                var configFacts = EnsureFacts(productId, workFlowStatusId);

                if (configFacts == null)
                    throw new NotFoundException($"No verification facts are found for {entityType}");

                foreach (var item in configFacts)
                {
                    if (item.Methods != null && item.Methods.Any())
                    {
                        foreach (var method in item.Methods)
                        {
                            foreach (var methodSource in method.Sources)
                            {
                                var documents = new List<string>();
                                if (methodSource.Type == SourceType.Form || methodSource.Type == SourceType.Review)
                                {
                                    string document;
                                    var result = false;
                                    if (methodSource.Settings.TryGetValue("DocumentRequired", out document))
                                    {
                                        var documentPending = pendingDocuments.FirstOrDefault(i => i.DocumentName.Contains(document));
                                        if (documentPending != null)
                                            result = true;
                                        documents.Add(document);
                                    }
                                    if (document != null)
                                        items.Add(new DocumentResponse { IsDynamic = false, FactName = item.Name, MethodName = method.Name, DocumentName = documents, IsRequested = result });
                                }
                            }
                        }
                    }
                }
                foreach (var dynamicFactDocument in pendingDocuments.Where(p => p.FactType == FactType.Dynamic))
                {
                    items.Add(new DocumentResponse { IsDynamic = true, FactName = dynamicFactDocument.FactName, MethodName = dynamicFactDocument.MethodName, DocumentName = dynamicFactDocument.DocumentName, IsRequested = true });
                }
            });
            return items;
        }

        public async Task EventRePublish(string entityType, string entityId, string workFlowStatusId)
        {
            var factsInfo = await VerificationFactRepository.Get(entityType, entityId, CurrentStatus.Initiated);
            if (factsInfo != null && factsInfo.Count > 0)
            {
                foreach (var item in factsInfo)
                {
                    await EventHubClient.Publish(new ManualFactVerificationInitiated(entityType, entityId, item.FactName));
                }
            }

            var methodsInfo = await VerificationMethodRepository.GetMethodDetails(entityType, entityId, null);
            if (methodsInfo != null && methodsInfo.Count > 0)
            {
                foreach (var item in methodsInfo)
                {
                    if (item.Documents != null && item.Documents.Count > 0)
                    {
                        var pendingDocuments = await GetPendingDocument(entityType, entityId, workFlowStatusId);
                        await EventHubClient.Publish(new VerificationDocumentReceived(entityType, entityId, item.FactName, pendingDocuments.Count, null, null)); // need to incorporate
                    }
                }
            }
            var factsInfoVerification = await VerificationFactRepository.GetFactDetails(entityType, entityId, null);
            if (factsInfoVerification != null && factsInfoVerification.Count > 0)
            {
                foreach (var item in factsInfoVerification)
                {
                    await EventHubClient.Publish(new FactVerificationCompleted { Fact = item });
                }
            }

        }

        public async Task InitiateDynamicFactVerification(string entityType, string entityId, string workFlowStatusId, IDynamicFactRequest dynamicFactRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entityId is required.");

            entityType = EnsureEntityType(entityType);
            if (dynamicFactRequest == null)
                throw new ArgumentException($"{nameof(dynamicFactRequest)} cannot be null");

            if (string.IsNullOrWhiteSpace(dynamicFactRequest.FactName))
                throw new ArgumentException($"{nameof(dynamicFactRequest.FactName)} cannot be null");

            if (dynamicFactRequest.DocumentDetails != null && !dynamicFactRequest.DocumentDetails.Any())
                throw new ArgumentException($"{nameof(dynamicFactRequest.DocumentDetails)} cannot be null");

            var productId = await GetProductId(entityType, entityId);
            var currentUser = EnsureCurrentUser();

            await ChangeFactStatus(entityType, entityId, productId, dynamicFactRequest.FactName, currentUser, CurrentStatus.Initiated, dynamicFactRequest.FactName, workFlowStatusId, null, dynamicFactRequest.DocumentDetails, true);

            if (dynamicFactRequest.DocumentDetails != null && dynamicFactRequest.DocumentDetails.Any())
                await EventHubClient.Publish("DynamicFactVerificationRequested", new { EntityType = entityType, EntityId = entityId, ProductId = productId, Fact = dynamicFactRequest });

        }

        #endregion

        #region Private Methods
        private Dictionary<string, string> EntityTypes { get; set; }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (EntityTypes == null)
                EntityTypes = LookupService.GetLookupEntries("entityTypes");

            if (!EntityTypes.ContainsKey(entityType))
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }

        private async Task<VerificationResult> ExecuteUsingProductRule(IFact factDetails, IMethod method, string entityId, string entityType, string productId)
        {
            var secondaryResult = await SecondaryKeyCheck(entityId, entityType, factDetails, productId);

            var result = await ProductRuleService.RunRule(entityType, entityId, productId, method.Name, secondaryResult?.ResultDetail[0]);

            if (result == null)
                throw new VerificationFailedException("Unable to execute rule : " + method.Name);
            var ruleExecutionResult = result;
            if (ruleExecutionResult.ExceptionDetail != null && ruleExecutionResult.ExceptionDetail.Count > 0)
            {
                throw new VerificationFailedException("Unable to Verify method : " + method.Name);
            }

            if (ruleExecutionResult.Result == Result.Passed)
                return VerificationResult.Passed;
            if (ruleExecutionResult.Result == Result.UnDefined)
                return VerificationResult.None;
            if (ruleExecutionResult.Result == Result.Failed)
                return VerificationResult.Failed;

            return VerificationResult.UnableToSpecify;
        }

        private Tuple<VerificationResult, string, object> ExecuteRule(Dictionary<string, object> data, IMethod method, string entityId)
        {
            if (string.IsNullOrEmpty(method.VerificationRule?.Name) || string.IsNullOrEmpty(method.VerificationRule?.Version))
                throw new InvalidOperationException($"VerificationRule not set for {method.Name}");

            string isVerified;
            string reason = null;
            object interMediateData = null;
            if (method.VerificationRule.CanRuleReturnVerificationResult || Configuration.CanRuleReturnVerificationResult)
            {
                var executionResult = DecisionEngineService.Execute<dynamic, VerificationExecutionResult>(method.VerificationRule.Name, new { payload = new { data, applicationNumber = entityId } });
                if (executionResult != null)
                {
                    isVerified = executionResult.Result;
                    interMediateData = executionResult.Data;
                    reason = executionResult.Reason;
                }
                else
                {
                    throw new Exception("Response is null from rule");
                }
            }
            else
            {
                isVerified = DecisionEngineService.Execute<dynamic, string>(method.VerificationRule.Name, new { payload = new { data, applicationNumber = entityId } });
            }

            var verificationResult = VerificationResult.UnableToSpecify;

            if (isVerified != null && isVerified.ToLower() == "true")
                verificationResult = VerificationResult.Passed;
            if (isVerified != null && isVerified.ToLower() == "na")
                verificationResult = VerificationResult.None;
            if (isVerified != null && isVerified.ToLower() == "errored")
                verificationResult = VerificationResult.Errored;
            if (isVerified != null && isVerified.ToLower() == "false")
                verificationResult = VerificationResult.Failed;

            return new Tuple<VerificationResult, string, object>(verificationResult, reason, interMediateData);
        }

        private async Task<ProductRuleResult> SecondaryKeyCheck(string entityId, string entityType, IFact factDetails, string productId)
        {
            ProductRuleResult secondaryResult = null;
            if (Configuration.SecondaryKeyEnable && !string.IsNullOrEmpty(Configuration.SecondaryRuleName) && factDetails.IsSecondaryEnable)
            {
                secondaryResult = await ProductRuleService.RunRule(entityType, entityId, productId, Configuration.SecondaryRuleName);
            }

            return secondaryResult;
        }

        private bool CanExecuteRule(IMethod method, IVerificationMethod verificationMethod)
        {
            var totalSource = method.Sources.Count;
            var currentCount = verificationMethod.VerificationSources.Select(v => v.SourceName).Distinct().Count();
            return totalSource == currentCount;
        }

        private Dictionary<string, object> ExtractedDataSources(IVerificationMethod verificationMethod)
        {
            var extractedDataSources = new Dictionary<string, object>();

            var sources = verificationMethod.VerificationSources.GroupBy(r => r.SourceName).Select(g => g.OrderByDescending(x => x.DataExtractedDate.Time).First());

            foreach (var source in sources)
            {
                extractedDataSources.Add(source.SourceName, source.DataExtracted);
            }

            return extractedDataSources;
        }

        private async Task ChangeFactStatus(string entityType, string entityId, string productId, string factName, string currentUser, CurrentStatus status, string currentInitiateMethod, string workFlowStatusId, string pollingId = null, List<IDocumentDetail> documentDetails = null, bool isDynamic = false)
        {
            var verificationFact = await VerificationFactRepository.Get(entityType, entityId, factName);

            if (verificationFact == null)
            {
                verificationFact = new VerificationFacts()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ProductId = productId,
                    StatusWorkFlowId = workFlowStatusId,
                    FactName = factName,
                    CurrentStatus = status,
                    CurrentInitiatedMethod = currentInitiateMethod,
                    PollingId = pollingId,
                    FactType = isDynamic ? FactType.Dynamic : FactType.Default,
                    DocumentDetails = documentDetails
                };

            }
            else
            {
                if (verificationFact.VerificationStatus == VerificationStatus.Passed && verificationFact.CurrentStatus == CurrentStatus.Completed)
                    return;

                if (verificationFact.CurrentStatus == CurrentStatus.Initiated || status == CurrentStatus.Initiated || verificationFact.CurrentStatus == CurrentStatus.InProgress)
                {
                    verificationFact.CurrentStatus = status;
                    verificationFact.VerificationStatus = VerificationStatus.None;
                    verificationFact.CurrentInitiatedMethod = currentInitiateMethod;
                }
                if (verificationFact.CurrentStatus == CurrentStatus.Errored)
                {
                    verificationFact.VerificationStatus = VerificationStatus.None;
                    verificationFact.CurrentInitiatedMethod = "";
                }

            }
            await VerificationFactRepository.AddOrUpdate(verificationFact);
        }

        private static IMethod EnsureFactMethod(IFact fact, string methodName)
        {
            var method = fact.Methods.FirstOrDefault(m => m.Name.Equals(methodName, StringComparison.OrdinalIgnoreCase));
            if (method == null)
                throw new NotFoundException($"{methodName} is not found for {fact.Name} in configuration.");
            return method;
        }

        private Fact EnsureFact(string workFlowStatusId, string factName, string productId)
        {
            if (string.IsNullOrWhiteSpace(workFlowStatusId))
                throw new ArgumentException("currentStatusFlow is required.");

            if (string.IsNullOrWhiteSpace(factName))
                throw new ArgumentException("FactName is required.");

            var facts = EnsureFacts(productId, workFlowStatusId);

            var configFact = facts.FirstOrDefault(e => e.Name.Equals(factName, StringComparison.OrdinalIgnoreCase));

            if (configFact == null)
                throw new NotFoundException($"The {factName} not found for {workFlowStatusId}");

            return configFact;
        }

        private List<Fact> EnsureFacts(string productId, string workFlowStatusId)
        {
            if (Configuration == null)
                throw new NotFoundException("Configuration is not set.");

            CaseInsensitiveDictionary<List<Fact>> productConfiguration;
            if (!Configuration.Facts.TryGetValue(productId, out productConfiguration))
                throw new NotFoundException($"The product not found in configuration for {productId}");
            List<Fact> statusWorkFlowName;
            if (!productConfiguration.TryGetValue(workFlowStatusId, out statusWorkFlowName))
                throw new NotFoundException($"The {workFlowStatusId} not found for {productId}");
            return statusWorkFlowName;
        }

        private static string GetResultValueForProductId(dynamic data)
        {
            Func<dynamic> resultProperty = () => data[0].ProductId;
            var productId = HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
            if (productId == null || string.IsNullOrWhiteSpace(productId.ToString()))
            {
                Func<dynamic> resultProperty1 = () => data[0].productId;
                return HasProperty(resultProperty1) ? GetValue(resultProperty1) : string.Empty;
            }
            else
            {
                return productId;
            }
        }

        private string EnsureCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("User is not authorized");
            return username;
        }

        private static string GetResultValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data[0].result;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static string GetResultValueForDynamic(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.result;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

        private static string GetFactValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.fact;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        #endregion
    }
}