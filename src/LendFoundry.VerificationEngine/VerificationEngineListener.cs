﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.VerificationEngine.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.StatusManagement.Client;
using LendFoundry.StatusManagement;
using LendFoundry.Foundation.Lookup;
using System.Text;
using LendFoundry.Foundation.Listener;
using Microsoft.CSharp.RuntimeBinder;

namespace LendFoundry.VerificationEngine
{
    public class VerificationEngineListener : ListenerBase, IVerificationEngineListener
    {
        public VerificationEngineListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IVerificationEngineServiceFactory verificationEngineServiceFactory,
            ILookupClientFactory lookupFactory,
            IStatusManagementServiceFactory entityStatusService)
            : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            EventHubFactory = eventHubFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            VerificationEngineServiceFactory = verificationEngineServiceFactory;
            LookupClientFactory = lookupFactory;
            EntityStatusService = entityStatusService;
        }

        #region Private Properties

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IVerificationEngineServiceFactory VerificationEngineServiceFactory { get; }
        private ILookupClientFactory LookupClientFactory { get; }
        private IStatusManagementServiceFactory EntityStatusService { get; }


        #endregion

        #region Public Methods

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName, null, "system", null);
            var reader = new StaticTokenReader(token.Value);
            var configuration = ConfigurationFactory.Create<VerificationConfiguration>(Settings.ServiceName, reader).Get();
            if (configuration == null || configuration.Events == null || !configuration.Events.Any())
                return null;
            else
            {
                return configuration.Events.Select(x => x.Name).Distinct().ToList<string>();
            }
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var serviceName = Settings.ServiceName;
            var token = TokenHandler.Issue(tenant, serviceName, null, "system", null);
            var reader = new StaticTokenReader(token.Value);
            var eventHub = EventHubFactory.Create(reader);
            var configuration = ConfigurationFactory.Create<VerificationConfiguration>(Settings.ServiceName, reader).Get();
            var verificationService = VerificationEngineServiceFactory.Create(reader, TokenHandler, logger, configuration);
            var statusService = EntityStatusService.Create(reader);
            var lookupService = LookupClientFactory.Create(reader);

            if (configuration == null || configuration.Events == null || !configuration.Events.Any())
            {
                logger.Info($"The configuration for service #{serviceName} could not be found for {tenant}, please verify");
                return null;
            }
            else
            {
                var uniqueEvents = configuration.Events.Distinct().ToList();

                logger.Info($"Total of ({uniqueEvents.Count}) rules found for tenant {tenant}");

                uniqueEvents.ToList()
                            .ForEach(events =>
                            {
                                eventHub.On(events.Name, ProcessEvent(events, configuration, logger, lookupService, verificationService, statusService));
                                logger.Info($"It was made subscription to EventHub for the Event: #{events.Name} for {tenant}");
                            });

                return uniqueEvents.Select(x => x.Name).Distinct().ToList<string>();
            }
        }

        #endregion

        #region Private Methods
        private Action<EventInfo> ProcessEvent
        (
            EventMapping eventConfiguration,
            VerificationConfiguration configuration,
            ILogger logger,
            ILookupService lookupService,
            IVerificationEngineServiceInternal verificationService,
            IEntityStatusService entityStatusService
        )
        {
            return @event =>
           {
               logger.Info($"Processing {@event.Name} with Id - {@event.Id} for {@event.TenantId}");

               try
               {
                    var entityId = Convert.ToString(InterpolateExtentions.GetPropertyValue(eventConfiguration.EntityId.Replace("{", "").Replace("}", ""), @event));
                    var entityType = Convert.ToString(InterpolateExtentions.GetPropertyValue(eventConfiguration.EntityType.Replace("{", "").Replace("}", ""), @event));
                    object data;
                    if(@event.Name == "DocumentAdded")
                    {
                        var responseData = eventConfiguration.Response.FormatWith(@event);
                        var document = responseData != null ? Newtonsoft.Json.JsonConvert.DeserializeObject<LendFoundry.DocumentManager.Document>(responseData) : null;
                        if(document == null)
                        {
                            logger.Info($"Unable to process document details");
                        }
                        else
                        {
                            string factName = GetFactValue(document.Metadata);
                            string methodName = GetMethodValue(document.Metadata);
                            var workFlow = entityStatusService.GetActiveStatusWorkFlow(entityType, entityId).Result;
                            if (workFlow != null)
                            {
                                var workFlowStatusId = workFlow.StatusWorkFlowId;
                                verificationService.AddDocumentDetails(entityType, entityId, workFlowStatusId, factName, methodName, document).Wait();
                            }
                        }
                    }
                    else
                    {
                         var syndicationName = eventConfiguration.SyndicationName;
                         var referenceNumber = Convert.ToString(InterpolateExtentions.GetPropertyValue(eventConfiguration.ReferenceNumber.Replace("{", "").Replace("}", ""), @event));
                         var sourceType = string.Empty;
                         if (!string.IsNullOrWhiteSpace(eventConfiguration.SourceType))
                         {
                             sourceType = Convert.ToString(InterpolateExtentions.GetPropertyValue(eventConfiguration.SourceType.Replace("{", "").Replace("}", ""), @event));
                         }
                         if (!string.IsNullOrWhiteSpace(eventConfiguration.Response))
                         {
                             var responseData = eventConfiguration.Response.FormatWith(@event);
                             data = responseData != null ? Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData) : null;
                         }
                         else
                         {
                             data = @event.Data;
                         }

                         var workFlow = entityStatusService.GetActiveStatusWorkFlow(entityType, entityId).Result;
                         if (workFlow != null)
                         {
                             var workFlowStatusId = workFlow.StatusWorkFlowId;

                             if (string.IsNullOrWhiteSpace(entityType))
                                 throw new ArgumentException($"{nameof(entityType)} is mandatory");

                             entityType = entityType.ToLower();

                             if (lookupService.GetLookupEntry("entityTypes", entityType) == null)
                                 throw new ArgumentException($"{entityType} is not a valid entity");
                             var productId = verificationService.GetProductId(entityType, entityId).Result;

                             if (sourceType != SourceType.Polling.ToString())
                             {
                                 var facts = EnsureFacts(productId, workFlowStatusId, configuration);

                                 VerifyFact(facts, verificationService, logger, entityId, syndicationName, entityType,
                                     productId,
                                     workFlowStatusId, new { data }).Wait();
                             }
                             else
                             {
                                 var factdetails = verificationService.GetFactDetailsByPollingId(entityType, entityId, productId, referenceNumber).Result;
                                 if (factdetails != null)
                                     verificationService.Verify(entityType, entityId, workFlowStatusId, factdetails.FactName, factdetails.CurrentInitiatedMethod, factdetails.CurrentInitiatedMethod, false, productId, new { data }).Wait();
                             }

                             logger.Info($"Fact verified for {syndicationName} for {entityId} for {@event.TenantId} ");
                         }
                         else
                         {
                             logger.Error($"Could not Proccess the event :EntityId {entityId}", null, @event);

                         }
                    }                  
               }
               catch (Exception ex)
               {
                   logger.Error($"Unhandled exception while listening event {@event.Name}", ex, @event);
               }
           };
        }

        private static async Task VerifyFact(IEnumerable<Fact> facts, IVerificationEngineServiceInternal verificationService, ILogger logger, string entityId, string syndicationName, string entityType, string productId, string workFlowStatusId, object data)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is required");

            if (string.IsNullOrWhiteSpace(syndicationName))
                throw new ArgumentException("Syndication Name is required");

            foreach (var fact in facts)
            {
                if (fact.Methods == null) continue;

                foreach (var method in fact.Methods)
                {
                    if (method.Sources == null)
                        continue;

                    foreach (var source in method.Sources)
                    {
                        try
                        {
                            if ((source.Type != SourceType.Syndication && source.Type != SourceType.Polling) || !source.Name.Equals(syndicationName))
                                continue;

                            await verificationService.Verify(entityType, entityId, workFlowStatusId, fact.Name,
                                method.Name, source.Name, false, productId, new { data });

                        }
                        catch (Exception ex)
                        {
                            logger.Error($"Unhandled exception while verifying fact {fact.Name}", ex);
                        }
                    }
                }
            }

        }
        private static IEnumerable<Fact> EnsureFacts(string productId, string currentStatusFlowName, VerificationConfiguration configuration)
        {
            if (configuration == null)
                throw new NotFoundException("Configuration is not set.");

            CaseInsensitiveDictionary<List<Fact>> productConfiguration;
            if (!configuration.Facts.TryGetValue(productId, out productConfiguration))
                throw new NotFoundException($"The product not found in configuration for {productId}");
            List<Fact> statusWorkFlowName;
            if (!productConfiguration.TryGetValue(currentStatusFlowName, out statusWorkFlowName))
                throw new NotFoundException($"The {currentStatusFlowName} not found for {productId}");
            return statusWorkFlowName;
        }

        private static string GetFactValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.fact;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static string GetMethodValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.method;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

        #endregion
    }
}