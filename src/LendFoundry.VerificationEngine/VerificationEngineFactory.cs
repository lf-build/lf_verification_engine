﻿using System;
using LendFoundry.VerificationEngine.Configuration;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

using LendFoundry.DataAttributes.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.Voting.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.StatusManagement.Client;
using LendFoundry.EventHub;

namespace LendFoundry.VerificationEngine
{
    public class VerificationEngineServiceFactory : IVerificationEngineServiceFactory
    {
        public VerificationEngineServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public IVerificationEngineServiceInternal Create(ITokenReader reader, ITokenHandler handler, ILogger logger, VerificationConfiguration verificationConfiguration)
        {
            
            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTimeService = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var productRuleFactory = Provider.GetService<IProductRuleServiceClientFactory>();
            var productRuleService = productRuleFactory.Create(reader);          

            var lookUpFactory = Provider.GetService<ILookupClientFactory>();
            var lookUpService = lookUpFactory.Create(reader);

            var documentManagerFactory = Provider.GetService<IDocumentManagerServiceFactory>();
            var documentManagerService = documentManagerFactory.Create(reader);

            var factRepositoryFactory = Provider.GetService<IVerificationFactsRepositoryFactory>();
            var factRepository = factRepositoryFactory.Create(reader);

            var methodRepositoryFactory = Provider.GetService<IVerificationMethodRepositoryFactory>();
            var methodRepository = methodRepositoryFactory.Create(reader);

            var methodHistoryRepositoryFactory = Provider.GetService<IVerificationMethodHistoryRepositoryFactory>();
            var methodHistoryRepository = methodHistoryRepositoryFactory.Create(reader);

            var datatAttribute = Provider.GetService<IDataAttributesClientFactory>();
            var datatAttributeService = datatAttribute.Create(reader);

            var identityServiceFactory = Provider.GetService<IIdentityServiceFactory>();
            var identityEngineService = identityServiceFactory.Create(reader);

            var decisionEngineFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngineService = decisionEngineFactory.Create(reader);


            var votingServiceFactory = Provider.GetService<IVotingServiceFactory>();
            var votingService = votingServiceFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusManagementService = statusManagementServiceFactory.Create(reader);


            return new VerificationEngineService(factRepository, eventHubService, tenantTimeService, documentManagerService, 
                verificationConfiguration, productRuleService, lookUpService, identityEngineService, reader, 
                handler, methodRepository, datatAttributeService, methodHistoryRepository, votingService, 
                decisionEngineService, statusManagementService);
        }
    }
}
