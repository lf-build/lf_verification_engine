﻿using System;

namespace LendFoundry.VerificationEngine
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}
