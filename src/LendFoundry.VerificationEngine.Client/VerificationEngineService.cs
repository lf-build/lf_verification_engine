﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using LendFoundry.VerificationEngine.Configuration;
using System;
using LendFoundry.DocumentManager;
using Newtonsoft.Json;
using System.Linq;

namespace LendFoundry.VerificationEngine.Client
{
    public class VerificationEngineService : IVerificationEngineService
    {
        #region Constructors
        public VerificationEngineService(IServiceClient client)
        {
            Client = client;
        }
        #endregion

        #region Private Properties
        private IServiceClient Client { get; }
        #endregion


        public async Task<IVerificationMethod> Verify(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, string sourceName, object verificationRequest)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/{fact}/{method}/{source}", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddUrlSegment("fact", factName);
            request.AddUrlSegment("method", methodName);
            request.AddUrlSegment("source", sourceName);

            var json = JsonConvert.SerializeObject(verificationRequest);
            request.AddParameter("text/json", json, ParameterType.RequestBody);
            return await Client.ExecuteAsync<VerificationMethod>(request);
        }

        public async Task<IVerificationFacts> GetStatus(string entityType, string entityId, string workFlowStatusId, string factName)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/{fact}/status", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddUrlSegment("fact", factName);
            return await Client.ExecuteAsync<VerificationFacts>(request);
        }

        public async Task<List<IVerificationMethod>> GetDetails(string entityType, string entityId, string workFlowStatusId, string factName)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/{fact}/detail", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("fact", factName);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            var result = await Client.ExecuteAsync<List<VerificationMethod>>(request);
            return new List<IVerificationMethod>(result);
        }

        public async Task<List<IFact>> GetFactList(string productId, string workFlowStatusId)
        {
            var request = new RestRequest("{productId}/{workFlowStatusId}/config-detail", RestSharp.Method.GET);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            var result = await Client.ExecuteAsync<List<Fact>>(request);
            return new List<IFact>(result);
        }

        public async Task<List<IFactResponse>> GetFactVerification(string entityType, string entityId, string productId = null, bool doNotLoadMethods = false)
        {
            string uri = string.Empty;
            if (doNotLoadMethods)
            {
                if (!string.IsNullOrEmpty(productId))
                    uri = "{entityType}/{entityId}/do-not-load-methods/{productId}";
                else
                    uri = "{entityType}/{entityId}/do-not-load-methods";
            }
            else
            {
                if (!string.IsNullOrEmpty(productId))
                    uri = "{entityType}/{entityId}/{productId}";
                else
                    uri = "{entityType}/{entityId}";
            }
            var request = new RestRequest(uri, RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            if (!string.IsNullOrEmpty(productId))
                request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<List<FactResponse>>(request);
            return new List<IFactResponse>(result);
        }

        public async Task InitiateManualVerification(string entityType, string entityId, string workFlowStatusId, string factName, string currentInitiateMethod)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/{fact}/{methodName}/initiate-manualverification", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddUrlSegment("fact", factName);
            request.AddUrlSegment("methodName", currentInitiateMethod);
            await Client.ExecuteAsync(request);
        }

        public async Task<List<IRequiredDocumentResponse>> GetRequiredDocument(string productId, string workFlowStatusId, string factName)
        {
            var request = new RestRequest("{productId}/{workFlowStatusId}/{fact}/required-document", RestSharp.Method.GET);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddUrlSegment("fact", factName);
            var result = await Client.ExecuteAsync<List<RequiredDocumentResponse>>(request);
            return new List<IRequiredDocumentResponse>(result);
        }

        public async Task<List<IPendingDocumentResponse>> GetPendingDocument(string entityType, string entityId, string workFlowStatusId)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/pending-document", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            var result = await Client.ExecuteAsync<List<PendingDocumentResponse>>(request);
            return new List<IPendingDocumentResponse>(result);
        }

        public Task<string> GetProductId(string entityType, string entityId)
        {
            throw new NotImplementedException(); throw new NotImplementedException();
        }
        
        public async Task EventRePublish(string entityType, string entityId, string workFlowStatusId)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/events-republish", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            await Client.ExecuteAsync(request);
        }

        public async Task<List<IDocumentResponse>> GetDocuments(string entityType, string entityId, string workFlowStatusId)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/documents", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            var result = await Client.ExecuteAsync<List<IDocumentResponse>>(request);
            return new List<IDocumentResponse>(result);
        }

        public async Task<List<LendFoundry.DocumentManager.IDocument>> GetDocumentsByFact(string entityType, string entityId, string workFlowStatusId, string factName)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/{factName}/documentsDetails", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddUrlSegment("factName", factName);

            var result = await Client.ExecuteAsync<List<LendFoundry.DocumentManager.Document>>(request);
            return new List<LendFoundry.DocumentManager.IDocument>(result);
        }

        public async Task VerifyDocument(string entityType, string entityId, string workFlowStatusId, string factName, string documentId, string statusCode, List<string> reasons = null)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/{factName}/{documentId}/{status}/{*reasons}", RestSharp.Method.PUT);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddUrlSegment("factName", factName);
            request.AddUrlSegment("documentId", documentId);
            request.AddUrlSegment("status", statusCode);
            AppendReasons(request, reasons);
            await Client.ExecuteAsync(request);
        }

        private static void AppendReasons(IRestRequest request, IReadOnlyList<string> reasons)
        {
            if (reasons == null || !reasons.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < reasons.Count; index++)
            {
                var tag = reasons[index];
                url = url + $"/{{reason{index}}}";
                request.AddUrlSegment($"reason{index}", tag);
            }
            request.Resource = url;
        }

        public async Task<IVerificationFacts> GetFactDetailsByPollingId(string entityType, string entityId, string productId, string pollingId)
        {
            var request = new RestRequest("{entityType}/{entityId}/{productId}/{pollingId}/poll", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("pollingId", pollingId);
            return await Client.ExecuteAsync<VerificationFacts>(request);

        }

        public async Task ReSetVerification(string entityType, string entityId, IResetFactRequest resetFacts)
        {
            var request = new RestRequest("{entityType}/{entityId}/resetfacts/", RestSharp.Method.PUT);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(resetFacts);
            await Client.ExecuteAsync(request);
            
        }


        private static void AppendFacts(IRestRequest request, IReadOnlyList<string> factNames)
        {
            if (factNames == null || !factNames.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < factNames.Count; index++)
            {
                var tag = factNames[index];
                url = url + $"/{{reason{index}}}";
                request.AddUrlSegment($"factNames{index}", tag);
            }
            request.Resource = url;
        }

        public async Task InitiateSyndicationVerification(string entityType, string entityId, string workFlowStatusId, string factName, string currentInitiateMethod)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/{fact}/{methodName}/initiate-syndication-verification", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddUrlSegment("fact", factName);
            request.AddUrlSegment("methodName", currentInitiateMethod);
            await Client.ExecuteAsync(request);
        }

        public async Task InitiateDynamicFactVerification(string entityType, string entityId, string workFlowStatusId, IDynamicFactRequest dynamicFactRequest)
        {
            var request = new RestRequest("{entityType}/{entityId}/{workFlowStatusId}/initiate-dynamicfact-verification", RestSharp.Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("workFlowStatusId", workFlowStatusId);
            request.AddJsonBody(dynamicFactRequest);
            await Client.ExecuteAsync(request);
        }
    }
}
