﻿using System.Collections.Generic;

namespace LendFoundry.VerificationEngine.Configuration
{
    public interface ISource
    {
        SourceType Type { get; set; }
        string Name { get; set; }
        List<string> RequiredDataAttributes { get; set; }
        Dictionary<string, string> Settings { get; set; }
        string SaveToDataAttribute { get; set; }
        string InitiationRule { get; set; }
        string ExtractRule { get; set; }
        int MaxRetryForSyndicationRule { get; set; }
        int DurationOfRetryForSyndication { get; set; }
    }
}