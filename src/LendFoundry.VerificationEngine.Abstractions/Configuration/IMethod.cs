﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.VerificationEngine.Configuration
{
    public interface IMethod
    {
        string Name { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISource, Source>))]
        List<ISource> Sources { get; set; }    
        string GroupName { get; set; }
        bool IsReviewMethod { get; set; }
        bool InitiateReviewonFailure { get; set; }
        List<string> RolesForAccess { get; set; }
        VerificationRule VerificationRule { get; set; }
        Polling Poll { get; set; }
        List<string> DocumentTags { get; set; }
    }
}