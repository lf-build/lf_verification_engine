﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.VerificationEngine.Configuration
{
    public class Fact : IFact
    {
        public Fact()
        {
            Methods = new List<IMethod>();
        }
        public string Name { get; set; }
        public bool IsSecondaryEnable { get; set; }
        public bool IsOptional { get; set; }
        public string OptionalFactGroupName { get; set; }       
        [JsonConverter(typeof(InterfaceListConverter<IMethod, Method>))]
        public List<IMethod> Methods { get; set; }

      
    }
}
