﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.VerificationEngine.Configuration
{
    public interface IFact
    {
        string Name { get; set; }
        bool IsSecondaryEnable { get; set; }
        bool IsOptional { get; set; }
        string OptionalFactGroupName { get; set; }      
        [JsonConverter(typeof(InterfaceListConverter<IMethod, Method>))]
        List<IMethod> Methods { get; set; }
    }
}