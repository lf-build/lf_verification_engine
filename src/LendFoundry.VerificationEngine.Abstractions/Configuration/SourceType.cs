﻿namespace LendFoundry.VerificationEngine.Configuration
{
    public enum SourceType
    {
        Syndication,
        Form,
        Automatic,
        Review,
        Polling
    }
}