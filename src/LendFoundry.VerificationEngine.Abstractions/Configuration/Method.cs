﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.VerificationEngine.Configuration
{
    public class Method : IMethod
    {
        public Method()
        {
            Sources = new List<ISource>();
        }

        public string Name { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISource, Source>))]
        public List<ISource> Sources { get; set; }
        public string VerificationRuleVersion { get; set; }
        public string GroupName { get; set; }
        public bool IsReviewMethod { get; set; }
        public bool InitiateReviewonFailure { get; set; }
        public List<string> RolesForAccess { get; set; }
        public VerificationRule VerificationRule { get; set; }
        public Polling Poll { get; set; }
        public List<string> DocumentTags { get; set; }
    }
}
