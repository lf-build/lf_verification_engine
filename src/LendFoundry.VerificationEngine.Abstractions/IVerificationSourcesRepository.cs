﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationMethodRepository : IRepository<IVerificationMethod>
    {


        Task<IVerificationMethod> Get(string entityType, string entityId, string factName, string methodName);
        Task<List<IVerificationMethod>> Get(string entityType, string entityId, string factName);
        Task<IVerificationMethod> AddSource(string entityType, string entityId, string productId, string statusWorkFlowId, string factName, string methodName, IVerificationSources verificationSource);
        Task<IVerificationMethod> AddAttempt(string id, IMethodAttemptResult attemptResult);
        Task<List<IVerificationMethod>> GetMethodDetails(string entityType, string entityId, string productId);
        Task<IVerificationMethod> AddDocuments(string entityType, string entityId, string productId, string statusWorkFlowId, string factName, string methodName, Document document);
        
        Task<List<IVerificationMethod>> GetPendingDocuments(string entityType, string entityId, DocumentStatus status);
        Task<IVerificationMethod> ResetMethod(IVerificationMethod verificationMethod);

      
    }
}