﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationMethodRepositoryFactory
    {
        IVerificationMethodRepository Create(ITokenReader reader);
    }
}