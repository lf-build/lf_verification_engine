﻿using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public interface IRequiredDocumentResponse
    {
        string MethodName { get; set; }
        List<string> Documents { get; set; }
    }
}