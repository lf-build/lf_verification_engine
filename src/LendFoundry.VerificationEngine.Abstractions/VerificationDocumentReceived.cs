﻿namespace LendFoundry.VerificationEngine
{
    public class VerificationDocumentReceived
    {
        public VerificationDocumentReceived()
        {
        }

        public VerificationDocumentReceived(string entityType, string entityId, string fact,int pendingDocumentcount,string verifiedBy,string documentName)
        {
            EntityType = entityType;
            EntityId = entityId;
            Fact = fact;
            PendingDocumentcount = pendingDocumentcount;         
            VerifiedBy = verifiedBy;
            DocumentName = documentName;

        }

        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string Fact { get; set; }

        public int PendingDocumentcount { get; set; }

        public string VerifiedBy { get; set; }

        public string DocumentName { get; set; }
    }
}