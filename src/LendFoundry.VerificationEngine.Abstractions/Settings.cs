using System;

namespace LendFoundry.VerificationEngine
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "verification-engine";
    }
}