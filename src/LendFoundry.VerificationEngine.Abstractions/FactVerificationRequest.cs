﻿namespace LendFoundry.VerificationEngine
{
    public class FactVerificationRequest : IFactVerificationRequest
    {
        public VerificationResult Result { get; set; }
        public object  Data { get; set; }
    }
}
