﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.VerificationEngine
{
    public class DynamicFactRequest : IDynamicFactRequest
    {
        public string FactName { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDocumentDetail, DocumentDetail>))]
        public List<IDocumentDetail> DocumentDetails { get; set; }
    }
}
