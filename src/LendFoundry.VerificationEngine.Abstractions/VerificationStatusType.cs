﻿
namespace LendFoundry.VerificationEngine
{
    public enum VerificationStatus
    {
        None=0,
        Failed = 1,
        Passed = 2,
        Errored=3
    }
}
