﻿namespace LendFoundry.VerificationEngine
{
    public class DocumentDetail : IDocumentDetail
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
