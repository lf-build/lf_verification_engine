﻿using System.Threading.Tasks;

namespace LendFoundry.VerificationEngine
{
    public interface IVerificationEngineServiceInternal : IVerificationEngineService
    {
        Task<IVerificationMethod> Verify(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, string sourceName, bool isManual, string productId,  object input);
        Task<LendFoundry.DocumentManager.IDocument> AddDocumentDetails(string entityType, string entityId, string workFlowStatusId, string factName, string methodName, LendFoundry.DocumentManager.IDocument documentInfo);
    }
}